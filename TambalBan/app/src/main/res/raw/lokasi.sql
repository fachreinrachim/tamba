create table lokasi
(
   id             INTEGER,
   address        TEXT,
   type           TEXT,
   open_hour      TEXT,
   close_hour     TEXT,
   tubles         TEXT,
   _24h           TEXT,
   nitrogen       TEXT,
   sale           TEXT,
   lat            NUMERIC,
   lon            NUMERIC,

   primary key (id)
);