package com.fachreinrachim.tambalban.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.fachreinrachim.tambalban.model.Filter;
import com.fachreinrachim.tambalban.model.Lokasi;
import com.fachreinrachim.tambalban.util.DBHelper;

import java.util.ArrayList;

/**
 * Created by Fachrein Rachim M on 5/9/2017.
 */

public class LokasiDAO extends DBHelper{
    public LokasiDAO(Context context) {
        super(context);
    }

    public static final String LOKASI_TABLE = "lokasi";

    public static final String LOKASI_ID = "id";
    public static final String LOKASI_ADDRESS = "address";
    public static final String LOKASI_TYPE = "type";
    public static final String LOKASI_OPEN_HOUR = "open_hour";
    public static final String LOKASI_CLOSE_HOUR = "close_hour";
    public static final String LOKASI_TUBLES = "tubles";
    public static final String LOKASI_24_HOUR = "_24h";
    public static final String LOKASI_NITROGEN = "nitrogen";
    public static final String LOKASI_SALE = "sale";
    public static final String LOKASI_LAT = "lat";
    public static final String LOKASI_LON = "lon";


    public boolean insertOrUpdateLokasi(Lokasi lokasi){
        long result = -1;

        try {
            this.openWriteDataBase();

            ContentValues contentValues = new ContentValues();

            contentValues.put(LOKASI_ID, lokasi.getId());
            contentValues.put(LOKASI_ADDRESS, lokasi.getAddress());
            contentValues.put(LOKASI_TYPE, lokasi.getType());
            contentValues.put(LOKASI_OPEN_HOUR, lokasi.getOpenHour());
            contentValues.put(LOKASI_CLOSE_HOUR, lokasi.getCloseHour());
            contentValues.put(LOKASI_TUBLES, lokasi.getTubles());
            contentValues.put(LOKASI_24_HOUR, lokasi.get24h());
            contentValues.put(LOKASI_NITROGEN, lokasi.getNitrogen());
            contentValues.put(LOKASI_SALE, lokasi.getSale());
            contentValues.put(LOKASI_LAT, lokasi.getLat());
            contentValues.put(LOKASI_LON, lokasi.getLon());

            result = getDb().replace(LOKASI_TABLE, null, contentValues);

        }catch(Exception e){
            e.printStackTrace();
        }finally {
            this.close();
        }

        if(result == -1){
            return false;
        }else{
            return true;
        }
    }

    public ArrayList<Lokasi> getAllLokasi(){
        ArrayList<Lokasi> lokasiList = new ArrayList<>();
        try{
            SQLiteDatabase db = this.getWritableDatabase();
            String[] args = new String[]{};
            String sql = "SELECT * FROM " + LOKASI_TABLE;
            Cursor res = db.rawQuery(sql, null);

            if (res.getCount() != 0) {// check is not null
                while (res.moveToNext()) {
                    Lokasi lokasi = new Lokasi();

                    lokasi.setId(res.getString(res.getColumnIndex(LOKASI_ID)));
                    lokasi.setAddress(res.getString(res.getColumnIndex(LOKASI_ADDRESS)));
                    lokasi.setType(res.getString(res.getColumnIndex(LOKASI_TYPE)));
                    lokasi.setOpenHour(res.getString(res.getColumnIndex(LOKASI_OPEN_HOUR)));
                    lokasi.setCloseHour(res.getString(res.getColumnIndex(LOKASI_CLOSE_HOUR)));
                    lokasi.setTubles(res.getString(res.getColumnIndex(LOKASI_TUBLES)));
                    lokasi.set24h(res.getString(res.getColumnIndex(LOKASI_24_HOUR)));
                    lokasi.setNitrogen(res.getString(res.getColumnIndex(LOKASI_NITROGEN)));
                    lokasi.setSale(res.getString(res.getColumnIndex(LOKASI_SALE)));
                    lokasi.setLat(res.getDouble(res.getColumnIndex(LOKASI_LAT)));
                    lokasi.setLon(res.getDouble(res.getColumnIndex(LOKASI_LON)));

                    lokasiList.add(lokasi);
                }
            }
            res.close();
            this.close();
        }catch(Exception e){
            e.printStackTrace();
        }finally {
            this.close();
        }

        return lokasiList;
    }

    public ArrayList<Lokasi> getAllLokasiFilter(Filter filter){
        ArrayList<Lokasi> lokasiList = new ArrayList<>();
        try{
            SQLiteDatabase db = this.getWritableDatabase();
            String[] args = new String[]{};
            String sql = "SELECT * FROM " + LOKASI_TABLE + " WHERE 1 " ;

            if (filter != null){

                sql += (filter.getJamBuka() != null ? " AND "+ LOKASI_OPEN_HOUR + " >= '"+ filter.getJamBuka()+"'" : "");
                sql += (filter.getJamTutup() != null ? " AND "+ LOKASI_CLOSE_HOUR + " <= '"+ filter.getJamTutup()+"'" : "");
                sql += (filter.is24Jam()? " AND "+ LOKASI_24_HOUR + " = '"+ Lokasi.LOKASI_YES+"'" : "");
                sql += (filter.isTubles()? " AND "+LOKASI_TUBLES + " = '"+ Lokasi.LOKASI_YES+"'" : "");
                sql += (filter.isTubles()? " AND "+LOKASI_NITROGEN + " = '"+ Lokasi.LOKASI_YES+"'" : "");

                if (filter.getTipeTambal() != null){
                    if (filter.getTipeTambal().equals(Lokasi.TYPE_MOTOR)){
                        sql += " AND "+ LOKASI_TYPE + " = '" + Lokasi.TYPE_MOTOR +"'";
                    }else if (filter.getTipeTambal().equals(Lokasi.TYPE_MOBIL)){
                        sql += " AND "+ LOKASI_TYPE + " = '" + Lokasi.TYPE_MOBIL+"'";
                    }
                }

            }

            Cursor res = db.rawQuery(sql, null);

            if (res.getCount() != 0) {// check is not null
                while (res.moveToNext()) {
                    Lokasi lokasi = new Lokasi();

                    lokasi.setId(res.getString(res.getColumnIndex(LOKASI_ID)));
                    lokasi.setAddress(res.getString(res.getColumnIndex(LOKASI_ADDRESS)));
                    lokasi.setType(res.getString(res.getColumnIndex(LOKASI_TYPE)));
                    lokasi.setOpenHour(res.getString(res.getColumnIndex(LOKASI_OPEN_HOUR)));
                    lokasi.setCloseHour(res.getString(res.getColumnIndex(LOKASI_CLOSE_HOUR)));
                    lokasi.setTubles(res.getString(res.getColumnIndex(LOKASI_TUBLES)));
                    lokasi.set24h(res.getString(res.getColumnIndex(LOKASI_24_HOUR)));
                    lokasi.setNitrogen(res.getString(res.getColumnIndex(LOKASI_NITROGEN)));
                    lokasi.setSale(res.getString(res.getColumnIndex(LOKASI_SALE)));
                    lokasi.setLat(res.getDouble(res.getColumnIndex(LOKASI_LAT)));
                    lokasi.setLon(res.getDouble(res.getColumnIndex(LOKASI_LON)));

                    lokasiList.add(lokasi);
                }
            }
            res.close();
            this.close();
        }catch(Exception e){
            e.printStackTrace();
        }finally {
            this.close();
        }

        return lokasiList;
    }


}
