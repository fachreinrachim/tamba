package com.fachreinrachim.tambalban.api;

import android.app.Activity;

import com.fachreinrachim.tambalban.dao.LokasiDAO;
import com.fachreinrachim.tambalban.interfaces.ApiInterface;
import com.fachreinrachim.tambalban.model.Lokasi;
import com.fachreinrachim.tambalban.rest.APIClient;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;

/**
 * Created by Fachrein Rachim M on 5/9/2017.
 */

public class LokasiAPI {
    private Activity activity;
    private ApiInterface mInterfaceService;

    private ArrayList<Lokasi> lokasiList;

    public LokasiAPI(Activity activity) {
        this.activity = activity;
        mInterfaceService = APIClient.getClient().create(ApiInterface.class);
    }

    public boolean getAllLokasi() {

        boolean res = true;

        lokasiList = new ArrayList<Lokasi>();

        Call<List<Lokasi>> call = mInterfaceService.getAllLokasi();

        try {
            lokasiList = (ArrayList<Lokasi>) call.execute().body();

            if (lokasiList != null)
                saveLokasiList();

        } catch (IOException e) {
            res = false;
            e.printStackTrace();
        }

        return res;
    }

    public void saveLokasiList() {
        LokasiDAO lokasiDAO = new LokasiDAO(activity);
        //insert pmoreder to sqlLite Database
        for (Lokasi lokasi: lokasiList) {
            boolean res = true;
            res = lokasiDAO.insertOrUpdateLokasi(lokasi);

            if (res == false) {
                break;
            }
        }
    }
}
