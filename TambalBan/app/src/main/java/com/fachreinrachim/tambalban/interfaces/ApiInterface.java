package com.fachreinrachim.tambalban.interfaces;

import com.fachreinrachim.tambalban.model.Lokasi;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Fachrein Rachim M on 5/9/2017.
 */

public interface ApiInterface {

    String LOKASI_TAMBAL_GET_ALL = "Lokasi/getAllLokasi";

    /* ---------------------------------------- Lokasi URL --------------------------------------------- */

    @GET(LOKASI_TAMBAL_GET_ALL)
    Call<List<Lokasi>> getAllLokasi();

    /* ----------------------------------------------- End --------------------------------------------------- */
}
