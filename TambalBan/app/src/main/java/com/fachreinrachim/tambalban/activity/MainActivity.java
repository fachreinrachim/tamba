package com.fachreinrachim.tambalban.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.fachreinrachim.tambalban.R;
import com.fachreinrachim.tambalban.api.LokasiAPI;
import com.fachreinrachim.tambalban.dao.LokasiDAO;
import com.fachreinrachim.tambalban.fragment.AboutFragment;
import com.fachreinrachim.tambalban.fragment.MainFragment;
import com.fachreinrachim.tambalban.model.Lokasi;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private TextView titleBar;
    private static final int REQUEST_INTERNET = 200;
    private ProgressDialog loadingSync;

    private ArrayList<Lokasi> lokasiList;

    private LokasiDAO lokasiDAO;
    private Toolbar toolbar;

    private FloatingActionButton fabCenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //REQUEST MAP PERMISSION
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, PackageManager.PERMISSION_GRANTED);
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION}, PackageManager.PERMISSION_GRANTED);
            }
        }

        // CHECK DATA IN LOCAL DB
        lokasiDAO = new LokasiDAO(MainActivity.this);

        lokasiList = new ArrayList<Lokasi>();
        lokasiList = lokasiDAO.getAllLokasi();

        if (lokasiList.size() == 0){
            new GetDataFromServerTask().execute();
        }

        if (getIntent().getSerializableExtra(Lokasi.LOKASI_CLASS) != null){
            lokasiList = (ArrayList<Lokasi>) getIntent().getSerializableExtra(Lokasi.LOKASI_CLASS);
        }

        init(lokasiList);
    }

    public void init(ArrayList<Lokasi> lokasiList){
//        titleBar = (TextView) findViewById(getResources().getIdentifier("action_bar_title", "id", getPackageName()));
//        titleBar.setText(R.string.main_bar_title);

        getSupportActionBar().setTitle(R.string.main_title);
        try{
            MainFragment mainFragment = new MainFragment(lokasiList);

            //getSupportFragmentManager().beginTransaction().add(R.id.view, mainFragment).commit();
            getSupportFragmentManager().beginTransaction().add(R.id.view, mainFragment).commitAllowingStateLoss();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void initFilter(ArrayList<Lokasi> lokasiList){

        getSupportActionBar().setTitle(R.string.main_title);
        try{
            MainFragment mainFragment = new MainFragment(lokasiList);

            getSupportFragmentManager().beginTransaction().replace(R.id.view, mainFragment).commitAllowingStateLoss();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.main, menu);
        getMenuInflater().inflate(R.menu.filter, menu);
        MenuItem itemAdd = menu.findItem(R.id.menu_filter);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (item.getItemId()) {
            case R.id.menu_filter:
                startActivity(new Intent(this, FilterActivity.class));
                break;
        }

        return true;

    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_lokasi) {
            getSupportActionBar().setTitle(R.string.main_title);
            try{
                MainFragment mainFragment = new MainFragment(lokasiList);

                getSupportFragmentManager().beginTransaction().replace(R.id.view, mainFragment).commitAllowingStateLoss();
            }catch (Exception e){
                e.printStackTrace();
            }
        } else if (id == R.id.nav_filter) {

            startActivity(new Intent(this, FilterActivity.class));

        } else if (id == R.id.nav_about) {
            getSupportActionBar().setTitle(R.string.about_title);
            try{
                AboutFragment aboutFragment = new AboutFragment();

                getSupportFragmentManager().beginTransaction().replace(R.id.view, aboutFragment).commitAllowingStateLoss();
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private class GetDataFromServerTask extends AsyncTask<Void, Void, Boolean>{

        LokasiAPI lokasiAPI;

        @Override
        protected void onPreExecute() {

            lokasiAPI = new LokasiAPI(MainActivity.this);

            loadingSync = new ProgressDialog(MainActivity.this);

            loadingSync.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            loadingSync.setIndeterminate(false);
            loadingSync.setCancelable(false);
            loadingSync.setTitle(getString(R.string.sync_title));
            loadingSync.setMessage(getString(R.string.loading_please_wait));
            loadingSync.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

            loadingSync.show();
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            if (!lokasiAPI.getAllLokasi())
                return false;

            return true;
        }

        @Override
        protected void onPostExecute(Boolean res) {
            loadingSync.dismiss();

            if (res) {
                Toast.makeText(MainActivity.this, R.string.sync_succeed, Toast.LENGTH_LONG).show();
                lokasiList = lokasiDAO.getAllLokasi();
                init(lokasiList);
            }else
                Toast.makeText(MainActivity.this, R.string.sync_failed, Toast.LENGTH_LONG).show();

        }
    }
}
