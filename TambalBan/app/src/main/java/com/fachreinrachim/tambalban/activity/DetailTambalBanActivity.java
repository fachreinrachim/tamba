package com.fachreinrachim.tambalban.activity;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.akexorcist.googledirection.DirectionCallback;
import com.akexorcist.googledirection.GoogleDirection;
import com.akexorcist.googledirection.constant.AvoidType;
import com.akexorcist.googledirection.model.Direction;
import com.akexorcist.googledirection.util.DirectionConverter;
import com.fachreinrachim.tambalban.R;
import com.fachreinrachim.tambalban.model.Lokasi;
import com.fachreinrachim.tambalban.util.GPSTracker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

import static com.fachreinrachim.tambalban.R.string.google_maps_key;
import static com.fachreinrachim.tambalban.R.string.google_maps_server_key;

public class DetailTambalBanActivity extends AppCompatActivity {

    private Lokasi lokasi;
    private MapView mMapView;
    private GPSTracker gps;
    private double latitude;
    private double longitude;
    private GoogleMap mMap;
    private LatLng currLatLngSite;
    private TextView txtalamat;
    private TextView txttipe;
    private TextView txtdesk;
    private TextView txtjam;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_lokasi_tambal_ban);
        getSupportActionBar().setTitle(R.string.detail_lokasi);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        txtalamat = (TextView) findViewById(R.id.txtalamat);
        txttipe = (TextView) findViewById(R.id.txttipe);
        txtdesk = (TextView) findViewById(R.id.txtdesk);
        txtjam = (TextView) findViewById(R.id.txtjam);

        mMapView = (MapView) findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);

        gps = new GPSTracker(DetailTambalBanActivity.this, DetailTambalBanActivity.this);
        gps.getLocation();
        latitude = gps.getLatitude();
        longitude = gps.getLongitude();

       if (getIntent().getSerializableExtra(Lokasi.LOKASI_CLASS) != null){
           lokasi = (Lokasi) getIntent().getSerializableExtra(Lokasi.LOKASI_CLASS);

           txtalamat.setText(lokasi.getAddress());

           if (lokasi.get24h().equals(Lokasi.LOKASI_NO))
               txtjam.setText(lokasi.getOpenHour() + " - " + lokasi.getCloseHour());
           else
               txtjam.setText("Buka 24 jam");

           txttipe.setText(lokasi.getType());
           txtdesk.setText("Fasilitas: jual " + lokasi.getSale() + " "
                   +(lokasi.getTubles().equals(Lokasi.LOKASI_YES) ? ", Tubles" : "")
                   +(lokasi.getNitrogen().equals(Lokasi.LOKASI_YES) ? ", Nitrogen" : ""));
       }

        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                mMap = googleMap;
                currLatLngSite = new LatLng(latitude, longitude);

                MarkerOptions marker = new MarkerOptions().position(currLatLngSite).title(getString(R.string.your_location));

                mMap.addMarker(marker);
                //mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currLatLngSite, 15));

                LatLng latLng = new LatLng(lokasi.getLat(), lokasi.getLon());
                BitmapDescriptor bitmap;

                if (lokasi.getType().equals(Lokasi.TYPE_MOTOR)){
                    bitmap = BitmapDescriptorFactory.fromResource(R.drawable.motor);
                }else{
                    bitmap = BitmapDescriptorFactory.fromResource(R.drawable.car);
                }

                String desc = "Buka: " + lokasi.getOpenHour() + " Tutup: " + lokasi.getCloseHour() + " Fasilitas:"
                        + (lokasi.getTubles().equals(Lokasi.LOKASI_YES) ? " Tubles " : "")
                        + (lokasi.getNitrogen().equals(Lokasi.LOKASI_YES) ? " Nitrogen " : "");

                mMap.addMarker(new MarkerOptions().position(latLng).title(lokasi.getAddress()).
                        snippet(desc).icon(bitmap));

                LatLngBounds.Builder builder = new LatLngBounds.Builder();
                builder.include(currLatLngSite);
                builder.include(latLng);
                LatLngBounds bounds = builder.build();
                mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 150));

                //show direction
                GoogleDirection.withServerKey(getResources().getString(google_maps_server_key).toString())
                        .from(currLatLngSite).to(latLng)
                        .avoid(AvoidType.FERRIES).avoid(AvoidType.HIGHWAYS)
                        .execute(new DirectionCallback() {
                            @Override
                            public void onDirectionSuccess(Direction direction, String rawBody) {
                                if (direction.isOK()){
                                    Toast.makeText(DetailTambalBanActivity.this, R.string.show_direction_succeed, Toast.LENGTH_LONG).show();

                                    ArrayList<LatLng> directionPositionList = direction.getRouteList().get(0).getLegList().get(0).getDirectionPoint();
                                    mMap.addPolyline(DirectionConverter.createPolyline(DetailTambalBanActivity.this, directionPositionList, 5, Color.RED));

                                }else{
                                    Toast.makeText(DetailTambalBanActivity.this, R.string.show_direction_failed, Toast.LENGTH_LONG).show();
                                }
                            }

                            @Override
                            public void onDirectionFailure(Throwable t) {
                                Toast.makeText(DetailTambalBanActivity.this, R.string.show_direction_failed, Toast.LENGTH_LONG).show();
                            }
                        });
            }
        });

        mMapView.onResume();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
