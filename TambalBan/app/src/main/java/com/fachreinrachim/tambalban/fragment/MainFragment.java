package com.fachreinrachim.tambalban.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.SyncStateContract;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.fachreinrachim.tambalban.R;
import com.fachreinrachim.tambalban.activity.DetailTambalBanActivity;
import com.fachreinrachim.tambalban.activity.FilterActivity;
import com.fachreinrachim.tambalban.model.Lokasi;
import com.fachreinrachim.tambalban.util.GPSTracker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * create an instance of this fragment.
 */
public class MainFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match


    private View rootview;
    private MapView mMapView;
    private GoogleMap mMap;
    private Marker mMarker;
    private LatLng currLatLngSite;
    private GPSTracker gps;
    private double latitude;
    private double longitude;

    private ArrayList<Lokasi> lokasiList;
    private FloatingActionButton fabCenter;

    public MainFragment() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public MainFragment(ArrayList<Lokasi> lokasiList) {
        this.lokasiList = lokasiList;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        rootview = inflater.inflate(R.layout.fragment_main, container, false);

        mMapView = (MapView) rootview.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);

        fabCenter = (FloatingActionButton) rootview.findViewById(R.id.fab_zoom);

        gps = new GPSTracker(getActivity(), getActivity());
        gps.getLocation();
        latitude = gps.getLatitude();
        longitude = gps.getLongitude();

        fabCenter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currLatLngSite, 15));
            }
        });

        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                mMap = googleMap;
                currLatLngSite = new LatLng(latitude, longitude);

                MarkerOptions marker = new MarkerOptions().position(currLatLngSite).title(getString(R.string.your_location));

                mMap.addMarker(marker);
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currLatLngSite, 15));

                if (lokasiList != null){
                    for (final Lokasi lokasi: lokasiList) {
                        LatLng latLng = new LatLng(lokasi.getLat(), lokasi.getLon());
                        BitmapDescriptor bitmap;

                        if (lokasi.getType().equals(Lokasi.TYPE_MOTOR)){
                           bitmap = BitmapDescriptorFactory.fromResource(R.drawable.motor);
                        }else{
                            bitmap = BitmapDescriptorFactory.fromResource(R.drawable.car);
                        }

                        String desc = "Buka: " + lokasi.getOpenHour() + " Tutup: " + lokasi.getCloseHour() + " Fasilitas:"
                                + (lokasi.getTubles().equals(Lokasi.LOKASI_YES) ? " Tubles " : "")
                                + (lokasi.getNitrogen().equals(Lokasi.LOKASI_YES) ? " Nitrogen " : "");

                        Marker lokasiMarker = mMap.addMarker(new MarkerOptions().position(latLng).
                                title(lokasi.getAddress()).
                                snippet(desc).icon(bitmap));
                        lokasiMarker.setTag(lokasi);

                        //Set on pin click listener
                        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                            @Override
                            public void onInfoWindowClick(Marker marker) {

                                //check if marker is not current position pin
                                if (!marker.getTitle().equals(String.valueOf(R.string.your_location))){

                                    Intent intent = new Intent(getActivity(), DetailTambalBanActivity.class);
                                    Lokasi clickedLokasi = (Lokasi) marker.getTag();

                                    intent.putExtra(Lokasi.LOKASI_CLASS, clickedLokasi);

                                    startActivity(intent);
                                }
                            }
                        });

                    }
                }
            }
        });

        mMapView.onResume();

        return rootview;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {


        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_filter:
                startActivity(new Intent(getActivity(), FilterActivity.class));
                break;
        }
        return true;
    }
}
