package com.fachreinrachim.tambalban;

import android.*;
import android.app.Application;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;

import com.fachreinrachim.tambalban.api.LokasiAPI;
import com.fachreinrachim.tambalban.util.DBHelper;

import java.io.IOException;

/**
 * Created by Fachrein Rachim M on 5/9/2017.
 */

public class TambalBanApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        //create local DB SQLLite if not exist
        DBHelper myDbHelper = new DBHelper(this);
        try {
            myDbHelper.createDataBase();


        } catch (IOException ioe) {
            throw new Error("Unable to create database");
        }


    }
}
