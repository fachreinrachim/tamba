package com.fachreinrachim.tambalban.rest;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Fachrein Rachim M on 5/9/2017.
 */

public class APIClient {

    //    public static final String BASE_URL = "http://rems.pikpun.net/api/";
    public static final String BASE_URL = "http://139.59.100.87/tambalban/api/";
    //public static final String BASE_URL = "http://192.168.200.10/rems/api/";
    //public static final String BASE_URL = "http://139.59.100.87/ems/api/"; //digital ocean
    //public static final String BASE_URL = "http://113.11.131.198/ems/api/";
    //public static final String BASE_URL = "http://192.168.100.20/rems/api/";

    //    public static final String SERVER_URL = "http://rems.pikpun.net/";
    public static final String SERVER_URL = "http://139.59.100.87/tambalban/";
    //public static final String SERVER_URL = "http://192.168.200.10/rems/";
    //public static final String SERVER_URL = "http://113.11.131.198/ems/";
    //public static final String SERVER_URL = "http://139.59.100.87/ems/";
    //public static final String SERVER_URL = "http://192.168.100.20/rems/";


    private static Retrofit retrofit = null;

    private static OkHttpClient httpClient = new OkHttpClient.Builder()
            .readTimeout(30, TimeUnit.SECONDS)
            .connectTimeout(30, TimeUnit.SECONDS)
            .build();;

    public static Retrofit getClient() {
        if (retrofit==null) {

            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

            OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

    public interface OnConnectionTimeoutListener {
        void onConnectionTimeout();
    }

    public static <S> S createService(Class<S> serviceClass) {
        return retrofit.create(serviceClass);
    }
}
