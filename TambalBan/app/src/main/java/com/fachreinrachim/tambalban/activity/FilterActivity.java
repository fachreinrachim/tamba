package com.fachreinrachim.tambalban.activity;

import android.app.Activity;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.location.Location;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.TimePicker;

import com.fachreinrachim.tambalban.R;
import com.fachreinrachim.tambalban.dao.LokasiDAO;
import com.fachreinrachim.tambalban.model.Filter;
import com.fachreinrachim.tambalban.model.Lokasi;
import com.fachreinrachim.tambalban.util.GPSTracker;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

public class FilterActivity extends AppCompatActivity {

    EditText editTextJamBuka;
    EditText editTextJamTutup;

    CheckBox checkBox24Jam;
    CheckBox checkBoxNitrogen;
    CheckBox checkBoxTubles;

    RadioGroup radioGroup;
    RadioButton radioMotor;
    RadioButton radioMotorMobil;
    RadioButton radioAll;

    SeekBar seekBarJarak;

    TextView textView;

    int range = 0;
    private Button btnFilter;

    AppCompatActivity activity;
    ArrayList<Lokasi> lokasiList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);

        editTextJamBuka = (EditText) findViewById(R.id.edit_jam_buka);
        editTextJamTutup = (EditText) findViewById(R.id.edit_jam_tutup);

        checkBox24Jam = (CheckBox) findViewById(R.id.check_24_jam);
        checkBoxNitrogen = (CheckBox) findViewById(R.id.check_nitrogen);
        checkBoxTubles= (CheckBox) findViewById(R.id.check_tubles);

        radioGroup = (RadioGroup) findViewById(R.id.radio_tipe);
        radioMotor = (RadioButton) findViewById(R.id.radio_motor);
        radioMotorMobil = (RadioButton) findViewById(R.id.radio_motor_mobil);
        radioAll = (RadioButton) findViewById(R.id.radio_all);

        seekBarJarak = (SeekBar) findViewById(R.id.seekbar_range);
        textView = (TextView) findViewById(R.id.txt_range);

        btnFilter = (Button) findViewById(R.id.btn_filter);

        getSupportActionBar().setTitle(R.string.filter);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        activity = this;

        editTextJamBuka.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;

                mTimePicker = new TimePickerDialog(FilterActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        editTextJamBuka.setText( selectedHour + ":" + selectedMinute);
                    }

                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Pilih waktu buka");
                mTimePicker.show();
            }
        });

        editTextJamTutup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;

                mTimePicker = new TimePickerDialog(FilterActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        editTextJamTutup.setText( selectedHour + ":" + selectedMinute);
                    }

                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Pilih waktu tutup");
                mTimePicker.show();
            }
        });

        seekBarJarak.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                range = progress;
                textView.setText(progress + " km");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });

        btnFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptFilter();
            }
        });

    }

    private void attemptFilter() {

        String jamBuka = editTextJamBuka.getText().toString();
        String jamTutup = editTextJamTutup.getText().toString();

        Filter filter = new Filter();

        if (!TextUtils.isEmpty(jamBuka)){
            filter.setJamBuka(jamBuka);
        }

        if (!TextUtils.isEmpty(jamTutup)){
            filter.setJamTutup(jamTutup);
        }

        filter.setIs24Jam((checkBox24Jam.isChecked() ? true : false));
        filter.setTubles((checkBoxTubles.isChecked() ? true : false));
        filter.setNitrogen((checkBoxNitrogen.isChecked() ? true : false));

        if (radioMotor.isChecked()){
            filter.setTipeTambal(Lokasi.TYPE_MOTOR);
        }else if(radioMotorMobil.isChecked()){
            filter.setTipeTambal(Lokasi.TYPE_MOBIL);
        }else{
            filter.setTipeTambal(Lokasi.TYPE_ALL);
        }

        filter.setJarakMaksimum(range);

        LokasiDAO lokasiDAO = new LokasiDAO(FilterActivity.this);

        lokasiList = lokasiDAO.getAllLokasiFilter(filter);

        if (filter.getJarakMaksimum() > 0){
            filterRadius();
        }

        MainActivity mainActivity = new MainActivity();

        Intent intent = new Intent(FilterActivity.this, MainActivity.class);
        intent.putExtra(Lokasi.LOKASI_CLASS, lokasiList);

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);

        //((MainActivity) activity).initFilter(lokasiList);

    }

    private void filterRadius(){
        GPSTracker gps = new GPSTracker(FilterActivity.this, FilterActivity.this);
        gps.getLocation();
        double latitude = gps.getLatitude();
        double longitude = gps.getLongitude();

        float distanceInMeters = 0;

        ArrayList<Integer> indexList = new ArrayList<>();
        ArrayList<Lokasi> lokasiListTemp = lokasiList;

       // for (Iterator<Lokasi> iterator = lokasiList.iterator(); iterator.hasNext(); ) {
        for (int i = 0; i < lokasiList.size(); i++) {

            Lokasi lokasi = lokasiList.get(i);

            if (lokasi.getLat() != null && lokasi.getLon() != null) {
                float[] results = new float[1];
                Location.distanceBetween(lokasi.getLat(), lokasi.getLon(), latitude, longitude, results);
                distanceInMeters = results[0];
            }

            boolean isInRadius = distanceInMeters < range * 1000;

            if (!isInRadius){
                lokasiListTemp.remove(lokasi);
            }

        }

        lokasiList.clear();
        lokasiList = lokasiListTemp;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
