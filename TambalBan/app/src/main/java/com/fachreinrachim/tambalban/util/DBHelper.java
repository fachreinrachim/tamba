package com.fachreinrachim.tambalban.util;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

import com.fachreinrachim.tambalban.R;

import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringWriter;

/**
 * Created by Fachrein Rachim M on 5/9/2017.
 */

public class DBHelper extends SQLiteOpenHelper {

    private static String DB_PATH = "/data/data/com.fachreinrachim.tambalban/databases/";
    private static String DB_NAME = "tambalban.db";

    private static String DB_LOCAL = "tambalban.sql";

    private final Context myContext;
    private SQLiteDatabase myDataBase;


    public DBHelper(Context context) {
        super(context, DB_PATH + DB_NAME, null, 1);
        this.myContext = context;
    }

    public SQLiteDatabase getDb(){
        return myDataBase;
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    /**
     * Creates a empty database on the system and rewrites it with your own database.
     * */
    public void createDataBase() throws IOException {

        boolean dbExist = checkDataBase();

        if(dbExist){
            //do nothing - database already exist
        }else{

            //By calling this method and empty database will be created into the default system path
            //of your application so we are gonna be able to overwrite that database with our database.
            this.getReadableDatabase();

            //copyDataBase();

            //initialize database
            createSchema();

        }

    }

    /**
     * Copies your database from your local assets-folder to the just created empty database in the
     * system folder, from where it can be accessed and handled.
     * This is done by transfering bytestream.
     * */
    private void copyDataBase() throws IOException{

        //Open your local db as the input stream
        InputStream myInput = myContext.getAssets().open(DB_LOCAL);

        // create a File object for the parent directory
        File wallpaperDirectory = new File(DB_PATH);
        // have the object build the directory structure, if needed.
        wallpaperDirectory.mkdirs();
        // create a File object for the output file
        File outputFile = new File(wallpaperDirectory, DB_NAME);
        // now attach the OutputStream to the file object, instead of a String representation
        FileOutputStream fos = new FileOutputStream(outputFile);

        //Open the empty db as the output stream
        OutputStream myOutput = new FileOutputStream(outputFile);

        //transfer bytes from the inputfile to the outputfile
        byte[] buffer = new byte[1024];
        int length;
        while ((length = myInput.read(buffer))>0){
            myOutput.write(buffer, 0, length);
        }

        //Close the streams
        myOutput.flush();
        myOutput.close();
        myInput.close();

    }

    /**
     * Check if the database already exist to avoid re-copying the file each time you open the application.
     * @return true if it exists, false if it doesn't
     */
    private boolean checkDataBase(){

        SQLiteDatabase checkDB = null;

        try{
            String myPath = DB_PATH + DB_NAME;
            File file = new File(myPath);
            if (file.exists() && !file.isDirectory()) {
                checkDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);
            }
        }catch(SQLiteException e){
            e.printStackTrace();
            //database does't exist yet.

        }

        if(checkDB != null){
            checkDB.close();
        }

        return checkDB != null ? true : false;
    }


    public void createSchema(){
        String contentSql = null;

        try {
            openWriteDataBase();
            myDataBase.execSQL(createString(R.raw.lokasi));

            close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String createString(int id) throws IOException {
        InputStream is = myContext.getResources().openRawResource(id);
        StringWriter writer = new StringWriter();
        IOUtils.copy(is, writer);
        return writer.toString();
    }

    public void openWriteDataBase() {

        //Open the database
        String myPath = DB_PATH + DB_NAME;
        myDataBase = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);

    }

    public void openReadDatabase(){
        //Open the database
        String myPath = DB_PATH + DB_NAME;
        myDataBase = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);
    }

    @Override
    public synchronized void close() {

        if (myDataBase != null)
            myDataBase.close();

        super.close();

    }
}
