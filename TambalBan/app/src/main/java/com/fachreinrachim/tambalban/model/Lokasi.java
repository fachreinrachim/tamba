package com.fachreinrachim.tambalban.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Fachrein Rachim M on 5/9/2017.
 */

public class Lokasi implements Serializable {

    public static final String TYPE_MOTOR = "Motor";
    public static final String TYPE_MOBIL = "Motor dan Mobil";
    public static final String TYPE_ALL = "All";

    public static final String LOKASI_CLASS = "Lokasi";

    public static final String LOKASI_YES = "Y";
    public static final String LOKASI_NO = "T";

    @SerializedName("id")
    private String id;

    @SerializedName("address")
    private String address;

    @SerializedName("type")
    private String type;

    @SerializedName("open_hour")
    private String openHour;

    @SerializedName("close_hour")
    private String closeHour;

    @SerializedName("tubles")
    private String tubles;

    @SerializedName("24h")
    private String _24h;

    @SerializedName("nitrogen")
    private String nitrogen;

    @SerializedName("sale")
    private String sale;

    @SerializedName("lat")
    private Double lat;

    @SerializedName("lon")
    private Double lon;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getOpenHour() {
        return openHour;
    }

    public void setOpenHour(String openHour) {
        this.openHour = openHour;
    }

    public String getCloseHour() {
        return closeHour;
    }

    public void setCloseHour(String closeHour) {
        this.closeHour = closeHour;
    }

    public String getTubles() {
        return tubles;
    }

    public void setTubles(String tubles) {
        this.tubles = tubles;
    }

    public String get24h() {
        return _24h;
    }

    public void set24h(String _24h) {
        this._24h = _24h;
    }

    public String getNitrogen() {
        return nitrogen;
    }

    public void setNitrogen(String nitrogen) {
        this.nitrogen = nitrogen;
    }

    public String getSale() {
        return sale;
    }

    public void setSale(String sale) {
        this.sale = sale;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }
}
