package com.fachreinrachim.tambalban.model;

/**
 * Created by Fachrein Rachim M on 6/10/2017.
 */

public class Filter {

    String jamBuka;
    String jamTutup;

    boolean is24Jam;
    boolean isTubles;
    boolean isNitrogen;

    String tipeTambal;

    float jarakMaksimum;

    public String getJamBuka() {
        return jamBuka;
    }

    public void setJamBuka(String jamBuka) {
        this.jamBuka = jamBuka;
    }

    public String getJamTutup() {
        return jamTutup;
    }

    public void setJamTutup(String jamTutup) {
        this.jamTutup = jamTutup;
    }

    public boolean is24Jam() {
        return is24Jam;
    }

    public void setIs24Jam(boolean is24Jam) {
        this.is24Jam = is24Jam;
    }

    public boolean isTubles() {
        return isTubles;
    }

    public void setTubles(boolean tubles) {
        isTubles = tubles;
    }

    public boolean isNitrogen() {
        return isNitrogen;
    }

    public void setNitrogen(boolean nitrogen) {
        isNitrogen = nitrogen;
    }

    public String getTipeTambal() {
        return tipeTambal;
    }

    public void setTipeTambal(String tipeTambal) {
        this.tipeTambal = tipeTambal;
    }

    public float getJarakMaksimum() {
        return jarakMaksimum;
    }

    public void setJarakMaksimum(float jarakMaksimum) {
        this.jarakMaksimum = jarakMaksimum;
    }
}
